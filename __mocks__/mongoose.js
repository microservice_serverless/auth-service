module.exports = {
  Schema: jest.fn().mockImplementation(() => {
    return {
      pre: jest.fn(),
      methods: jest.fn()
    };
  }),
  Error: {
    ValidationError: jest.fn()
  },
  model: jest.fn(),
  connect: jest.fn().mockResolvedValue(),
  set: jest.fn()
};
